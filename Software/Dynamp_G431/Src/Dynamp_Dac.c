/*!
 * \file   Dynamp.c
 * \brief  Brief description of the file
 * \author Javier Morales
 *
 * Detailed description of the file if needed
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */
#include "stm32g4xx_hal.h"

/* Specific libraries */
#include "Dynamp_Dac.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define ui8DAC_INPUT_ATT			11
#define fDAC_R_SENSE				0.05
#define ui16DAC_RES					4095
#define fDAC_REF		   			3
//#define fDAC_OFFSET			0.3

//-----------------------------------------------------------------------------
/* Type and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------
extern DAC_HandleTypeDef hdac1;

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Dynamp_DacInit(void)
{
	Dynamp_DacSetCurrent(0);
	HAL_DAC_Start(&hdac1, DAC_CHANNEL_1);
}

// Current in mA
void Dynamp_DacSetCurrent(uint16_t ui16Current)
{
	float fDacValue;

//	fDacValue = ui16Current * ui8DAC_INPUT_ATT * fDAC_R_SENSE * ui16DAC_RES / fDAC_REF;
	HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, ui16Current);
}
