/*!
 * \file   Dynamp.c
 * \brief  Main Dynamp module
 * \author Javier Morales
 *
 * Detailed description of the file if needed
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */
#include <stdio.h>
#include <string.h>

/* Manufacturer libraries */
#include "stm32g4xx_hal.h"

/* Specific libraries */
#include "Dynamp.h"
#include "Dynamp_Display.h"
#include "Dynamp_Adc.h"
#include "Dynamp_Dac.h"
#include "Dynamp_Encoder.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define PERIOD_BLINK			500
#define PERIOD_ADC_V			100
#define PERIOD_DAC_CURRENT		100
#define PERIOD_ENCODER 			100

#define ui8X_COORDINATE			0
#define ui8Y_COORDINATE			0

//-----------------------------------------------------------------------------
/* Type and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------
void Dynamp_IntToString(char* sString, uint16_t ui16Number);
void Dynamp_DisplayTask(void);
void Dynamp_CurrentTask(void);

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint32_t Gui32Now;
uint32_t Gui32MomentBlink;
uint32_t Gui32MomentAdcV;
uint32_t Gui32MomentDacCurrent;
uint32_t Gui32MomentEncoder;

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------

void Dynamp_FloatToString(char* sString, float fNumber)
{
	if (fNumber == 0) {
		memset(sString, '0', 6);
	} else {
		char sStringNull[7];
		sprintf(sStringNull, "%06.3f", fNumber);
		memcpy(sString, sStringNull, 6);
	}
}

void Dynamp_DisplayTask(void)
{
	float fVoltage;
	float fCurrent;
	char sVoltage[6];
	char sCurrent[6];
//	char sEncoderValue[4];

//	Dynamp_AdcGetStringVoltage(sVoltage);
//	Dynamp_DisplayText(ui8X_COORDINATE, ui8Y_COORDINATE, sVoltage);

	fVoltage = Dynamp_AdcGetVoltage();
	fCurrent = Dynamp_AdcGetCurrent();

	Dynamp_FloatToString(sVoltage, fVoltage);
	Dynamp_FloatToString(sCurrent, fCurrent);

	Dynamp_DisplayText(ui8X_COORDINATE, ui8Y_COORDINATE, sVoltage, 6);
	Dynamp_DisplayText(ui8X_COORDINATE, ui8Y_COORDINATE+20, sCurrent, 6);

	Dynamp_DisplayText(ui8X_COORDINATE+108, ui8Y_COORDINATE, "V", 1);
	Dynamp_DisplayText(ui8X_COORDINATE+108, ui8Y_COORDINATE+20, "A", 1);
	Dynamp_DisplayUpdate();
}

void Dynamp_CurrentTask(void)
{
	Dynamp_DacSetCurrent(2047);
}

void Dynamp_Init(void)
{
	Gui32Now = HAL_GetTick();
	Gui32MomentBlink = Gui32Now + PERIOD_BLINK;
	Gui32MomentAdcV = Gui32Now + PERIOD_ADC_V;
	Gui32MomentDacCurrent = Gui32Now + PERIOD_DAC_CURRENT;
	Gui32MomentEncoder = Gui32Now + PERIOD_ENCODER;

	Dynamp_DisplayInit();
	Dynamp_AdcInit();
	Dynamp_DacInit();
	Dynamp_EncoderInit();
}

void Dynamp_Main(void)
{
	Gui32Now = HAL_GetTick();

	if (Gui32Now >= Gui32MomentBlink) {
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
		Gui32MomentBlink = Gui32Now + PERIOD_BLINK;

	} else if (Gui32Now >= Gui32MomentAdcV) {
		Dynamp_DisplayTask();
		Gui32MomentAdcV = Gui32Now + PERIOD_ADC_V;

	} else if (Gui32Now >= Gui32MomentDacCurrent) {
		Dynamp_CurrentTask();
		Gui32MomentDacCurrent = Gui32Now + PERIOD_DAC_CURRENT;

	} else if (Gui32Now >= Gui32MomentEncoder) {
		Gui32MomentEncoder = Gui32Now + PERIOD_ENCODER;

	} else {
		HAL_Delay(1);
	}
}
