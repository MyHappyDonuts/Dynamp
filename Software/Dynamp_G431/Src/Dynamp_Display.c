/*!
 * \file   Dynamp.c
 * \brief  Brief description of the file
 * \author Javier Morales
 *
 * Detailed description of the file if needed
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */
#include <stdio.h>
#include <string.h>

/* Manufacturer libraries */
#include "stm32g4xx_hal.h"

/* Specific libraries */
#include "Dynamp_Display.h"
#include "ssd1306/ssd1306.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
//#define PERIOD_BLINK			100

//-----------------------------------------------------------------------------
/* Type and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
//uint32_t Gui32Now;
//uint32_t Gui32MomentBlink;

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Dynamp_DisplayInit(void)
{
	ssd1306_Init();
}

void Dynamp_DisplayText(uint8_t ui8X, uint8_t ui8Y, char* sText, uint8_t ui8Length)
{
	char sLengthString[ui8Length];
	memcpy(sLengthString, sText, ui8Length);
	ssd1306_SetCursor(ui8X, ui8Y);
	ssd1306_WriteString(sLengthString, Font_11x18, White);
}

void Dynamp_DisplayUpdate(void)
{
	ssd1306_UpdateScreen();
}
