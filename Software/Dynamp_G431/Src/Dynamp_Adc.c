/*!
 * \file   Dynamp.c
 * \brief  Brief description of the file
 * \author Javier Morales
 *
 * Detailed description of the file if needed
 */

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */
#include "stm32g4xx_hal.h"

/* Specific libraries */
#include "Dynamp_Adc.h"

//-----------------------------------------------------------------------------
/* Constants */
//-----------------------------------------------------------------------------
#define ui8ADC_VOLTAGE_GAIN			11
#define ui16ADC_VOLTAGE_RES			4095
#define fADC_VOLTAGE_REF			3
#define fADC_VOLTAGE_GAIN_ERROR		1.1232
#define fADC_VOLTAGE_OFFSET_ERROR	-0.2438

#define ui8ADC_CURRENT_GAIN			1.81818
#define ui16ADC_CURRENT_RES			4095
#define fADC_CURRENT_REF			3
#define fADC_CURRENT_GAIN_ERROR		1.1632
#define fADC_CURRENT_OFFSET_ERROR	-0.2438

#define ui8ADC_BUFF_LENGHT		128

//-----------------------------------------------------------------------------
/* Type and structures */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Private function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint32_t GaConvertedDataVoltage[ui8ADC_BUFF_LENGHT];
uint32_t GaConvertedDataCurrent[ui8ADC_BUFF_LENGHT];

//-----------------------------------------------------------------------------
/* Imported variables */
//-----------------------------------------------------------------------------
extern ADC_HandleTypeDef hadc1;

extern ADC_HandleTypeDef hadc2;
//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void Dynamp_AdcInit(void)
{
	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED);

	HAL_ADC_Start_DMA (&hadc1, GaConvertedDataVoltage, ui8ADC_BUFF_LENGHT);
	HAL_ADC_Start_DMA (&hadc2, GaConvertedDataCurrent, ui8ADC_BUFF_LENGHT);
}

void Dynamp_AdcGetStringVoltage(char* sVoltage)
{
//	uint16_t ui16AdcValue;
//	float GfVoltage;
//
//	ui16AdcValue = Dynamp_AdcGetVoltage();
//	GfVoltage = ui16AdcValue * fADC_VOLTAGE_REF / ui16ADC_VOLTAGE_RES * ui8ADC_VOLTAGE_GAIN;


}

float Dynamp_AdcGetVoltage(void)
{
	float fAdcSum = 0;
	float fAdcAverage = 0;
	float GfVoltage = 0;

	for (uint8_t i = 0; i < ui8ADC_BUFF_LENGHT; i++) {
		fAdcSum = fAdcSum + GaConvertedDataVoltage[i];
	}
	fAdcAverage = fAdcSum / ui8ADC_BUFF_LENGHT;

	GfVoltage = fAdcAverage * fADC_VOLTAGE_REF / ui16ADC_VOLTAGE_RES * ui8ADC_VOLTAGE_GAIN * fADC_VOLTAGE_GAIN_ERROR + fADC_VOLTAGE_OFFSET_ERROR;
//	GfVoltage = fAdcAverage * fADC_VOLTAGE_REF / ui16ADC_VOLTAGE_RES * fADC_VOLTAGE_GAIN_ERROR + fADC_VOLTAGE_OFFSET_ERROR;


	return GfVoltage;
}

float Dynamp_AdcGetCurrent(void)
{
	float fAdcSum = 0;
	float fAdcAverage = 0;
	float GfCurrent = 0;

	for (uint8_t i = 0; i < ui8ADC_BUFF_LENGHT; i++) {
		fAdcSum = fAdcSum + GaConvertedDataCurrent[i];
	}
	fAdcAverage = fAdcSum / ui8ADC_BUFF_LENGHT;

	GfCurrent = fAdcAverage * fADC_CURRENT_REF / ui16ADC_CURRENT_RES * ui8ADC_CURRENT_GAIN * fADC_CURRENT_GAIN_ERROR + fADC_CURRENT_OFFSET_ERROR;
//	GfCurrent = fAdcAverage * fADC_CURRENT_REF / ui16ADC_CURRENT_RES * fADC_CURRENT_GAIN_ERROR + fADC_CURRENT_OFFSET_ERROR;

	return GfCurrent;
}



void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
//	HAL_ADC_Start_DMA (&hadc1, GaConvertedDataVoltage, 16);
	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_1);
}
